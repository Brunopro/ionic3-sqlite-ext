import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { SQLite, SQLiteObject, SQLiteTransaction, SQLiteDatabaseConfig } from '@ionic-native/sqlite';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  storage:SQLite;
  itemList = [];

  constructor(
    public navCtrl: NavController,
    private sqlite: SQLite,
    private platform: Platform) {
  }

  ionViewDidLoad() {
    this.getDbAlreadyCreated();
  }

  private getDbAlreadyCreated() {
    this.platform.ready().then(() => {
      this.storage = new SQLite();
      this.storage.create({ name: "teste.db", location: 'default', createFromLocation: 1 })
        .then((db: SQLiteObject) => {
          db.executeSql("SELECT * FROM users", []).then((data) => {
                let rows = data.rows;
                for (let i = 0; i < rows.length; i++) {
                    this.itemList.push({
                        id: rows.item(i).id,
                        name: rows.item(i).name
                    });

                    console.log('itemList[',i,']: ', this.itemList);
                }


            }, (error) => {
                console.info("Unable to execute sql " + JSON.stringify(error));
            })
        }, (err) => {
            console.info("Error opening database: " + err);
        });
    });
  }

}
